function [ICE_Enable,Gen_Enable,Mot_Enable,ICE_RPM_dem,Gen_Trq_dem]=...
    control(Vehicle_Power_dem,Engine_RPM,SOC)

    k = 0.95;                           %电机效率修正系数
    efficiency_gen = 0.88;      %假定发电机效率恒定
    efficiency_mot = efficiencyfit_mot(Vehicle_Power_dem)*k;    %计算电机最优效率并修正
    Motor_Power_dem = Vehicle_Power_dem/efficiency_mot;     %计算电机需求功率
    Motor_Power_dem = Motor_Power_dem/1000;                     %单位换算，W —>kW
    BEFORE = evalin('base','BEFORE');                                           %读入历史信息
    %% 当电池SOC<=0.4时，进入充电模式
    if SOC <= 0.4
        ICE_Enable = 1;
        Gen_Enable = 1;
        Mot_Enable = 1;
        correction = 10;                %充电功率修正量
        Gen_Power_out = Motor_Power_dem+correction;         %计算发电机输出功率
        ICE_Power_out = Gen_Power_out/efficiency_gen;           %计算内燃机输出功率
        ICE_RPM_dem = speedfit(ICE_Power_out);                      %根据内燃机最优油耗线计算转速
        Gen_Trq_dem = ICE_Power_out/ICE_RPM_dem/2/pi*60*1000;       %计算发电机需求扭矩
        %更新历史信息
        assignin('base','BEFORE',[ICE_Enable,Gen_Enable,Mot_Enable,correction]); 
        
    %% 当电池SOC处于0.4-0.6时，保持历史状态
    elseif 0.4 < SOC && SOC <= 0.6
        %读取历史信息
        ICE_Enable = BEFORE(1);
        Gen_Enable = BEFORE(2);
        Mot_Enable = BEFORE(3);
        correction = BEFORE(4);
        Gen_Power_out = Motor_Power_dem+correction;         %计算发电机输出功率
        ICE_Power_out = Gen_Power_out/efficiency_gen;           %计算内燃机输出功率
        ICE_RPM_dem = speedfit(ICE_Power_out);                      %根据内燃机最优油耗线计算转速
        Gen_Trq_dem = ICE_Power_out/ICE_RPM_dem/2/pi*60*1000;       %计算发电机需求扭矩
        
    %% 当电池SOC>0.6时，分情况讨论
    else
        if Motor_Power_dem < 7             %若电机需求功率小于10kW，仅电池供能
            ICE_Enable = 0;
            Mot_Enable = 1;
            ICE_RPM_dem = 0;
            if Engine_RPM > 0
                Gen_Enable = 1;
                Gen_Trq_dem = 100;              %进行发电机制动能量回收，转矩可进行优化
            else
                Gen_Enable = 0;
                Gen_Trq_dem = 0;
            end
            %更新历史信息
            assignin('base','BEFORE',[ICE_Enable,Gen_Enable,Mot_Enable,BEFORE(4)]); 
            
        else                                                %若电机需求功率大于10kW，启动内燃机
            ICE_Enable = 1;
            Gen_Enable = 1;
            Mot_Enable = 1;
            correction = 0;                         %不进行修正，或修正量为负，防止电池过充
            Gen_Power_out = Motor_Power_dem+correction;         %计算发电机输出功率
            ICE_Power_out = Gen_Power_out/efficiency_gen;           %计算内燃机输出功率
            ICE_RPM_dem = speedfit(ICE_Power_out);                      %根据内燃机最优油耗线计算转速
            Gen_Trq_dem = ICE_Power_out/ICE_RPM_dem/2/pi*60*1000;       %计算发电机需求扭矩
            assignin('base','BEFORE',[ICE_Enable,Gen_Enable,Mot_Enable,correction]); 
        end
    end
end