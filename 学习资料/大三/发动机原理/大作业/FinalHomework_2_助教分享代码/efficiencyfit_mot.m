function efficiency_mot=efficiencyfit_mot(power)

    p1 =  -1.619e-32;
    p2 =   6.001e-27;
    p3 =  -8.709e-22;
    p4 =   6.108e-17;
    p5 =  -1.944e-12;
    p6 =    7.73e-09;
    p7 =    0.001113;
    p8 =       65.43;
    efficiency_mot = p1*power^7 + p2*power^6 +...
        p3*power^5 + p4*power^4 + p5*power^3 +...
        p6*power^2 + p7*power + p8;
    efficiency_mot = efficiency_mot/100;
end