function [ICE_Enable,Gen_Enable,Mot_Enable,ICE_RPM_dem,Gen_Trq_dem]=...
    control(Vehicle_Power_dem,Engine_RPM,SOC)
%Te_mot函数
function Te_mot=Tefit_mot(speed)
c1=-2.610794496480510e-09;
c2=2.793120265597548e-05;
c3=-0.047441244364490;
c4=49.809105875653266;
Te_mot=c1*speed^3+c2*speed^2+c3*speed+c4;
end

%Speed_APU函数
function Speed_APU=Speedfit_APU(power)
a1=0.009769477697987;
a2=-1.242612681057585;
a3=93.273116699739150;
a4=4.997037821772905e+02;
Speed_APU=a1*power^3+a2*power^2+a3*power+a4;
end
%Te_APU函数
function Te_APU=Tefit_APU(power)
a1=2.694141602152893e-04;
a2=-0.054492265442230;
a3=4.225957150056422;
a4=35.275913131625130;
Te_APU=a1*power^3+a2*power^2+a3*power+a4;

end
                                                  
    ef_gen = 0.88;                                                
    ef_mot = efficiencyfit_mot(Vehicle_Power_dem);                %计算电动机最优效率
    Motor_Power_dem = Vehicle_Power_dem/ef_mot;                   %计算电动机需求功率
    Motor_Power_dem = Motor_Power_dem/1000;                       
    BEFORE = evalin('base','BEFORE');                             %BEFORE读入历史信息保存原状态
   
         
   %% 当电池SOC<=0.30时，进入充电模式
    if SOC <= 0.30
        ICE_Enable = 1;
        Gen_Enable = 1;
        Mot_Enable = 1;
        correction = 10;                                         
        Gen_Power_out = Motor_Power_dem+correction;              %电机输出功率
        ICE_RPM_dem=Speedfit_APU(Gen_Power_out);                 %内燃机需求转速
        Gen_Trq_dem=Tefit_APU(Gen_Power_out);                    %电机所需扭矩
        ICE_Power_out = Gen_Power_out/ef_gen;                    %内燃机输出功率
        ICE_RPM_dem = speedfit(ICE_Power_out);                   %内燃机需求转速
        Gen_Trq_dem = ICE_Power_out/ICE_RPM_dem/2/pi*60*1000;    %电机需求扭矩
       
        %更新历史信息
        assignin('base','BEFORE',[ICE_Enable,Gen_Enable,Mot_Enable,correction]); 
        
    %% 当电池SOC处于0.30-0.65时，保持历史状态
    elseif 0.30 < SOC && SOC <= 0.65
        ICE_Enable = BEFORE(1);
        Gen_Enable = BEFORE(2);
        Mot_Enable = BEFORE(3);
        correction = BEFORE(4);
        Gen_Power_out = Motor_Power_dem+correction;                 
        ICE_RPM_dem=Speedfit_APU(Gen_Power_out);
        Gen_Trq_dem=Tefit_APU(Gen_Power_out);

        
    %% 当电池SOC>0.65时，有以下兩种情況
    else
        if Motor_Power_dem < 7           %驱动电机需求较小，电池直接供电
            ICE_Enable = 0;
            Mot_Enable = 1;
            ICE_RPM_dem = 0;
            if Engine_RPM > 0
                Gen_Enable = 1;
                Gen_Trq_dem =Tefit_mot(Engine_RPM);   
            else
                Gen_Enable = 0;
                Gen_Trq_dem = 0;
            end
            assignin('base','BEFORE',[ICE_Enable,Gen_Enable,Mot_Enable,BEFORE(4)]); 
            
        else                                        %驱动电机需求较小，内燃机带动电机工作
            ICE_Enable = 1;
            Gen_Enable = 1;
            Mot_Enable = 1;
            correction = 0;                         
            Gen_Power_out = Motor_Power_dem+correction;        
         ICE_RPM_dem=Speedfit_APU(Gen_Power_out);
        Gen_Trq_dem=Tefit_APU(Gen_Power_out);
            

            assignin('base','BEFORE',[ICE_Enable,Gen_Enable,Mot_Enable,correction]); 
        end
    end
end
