
%%参考任务一，做出最优燃油消耗曲线。可在后续步骤中直接提供需求功率下，发动机对应的最小油耗下的转速与转矩。
%%最优能量转换效率曲线用矩阵保存在工作空间中，方便控制函数直接取用，缩短仿真时间。
 
 excel1=xlsread('EMMAP.xlsx');%提取数据
 excel2=xlsread('2.0TD万有数据.xlsx');
 
 x=excel1(:,1);%电机数据
 y=excel1(:,2);
 z=excel1(:,3);

 xl=linspace(min(x),max(x),200);
 yl=linspace(min(y),max(y),200);
 [X,Y]=meshgrid(xl,yl);
 Z=griddata(x,y,z,X,Y);

 P=linspace(min(x)*min(y)*pi/30000,120,30);%计算每个网格点的功率，并求每个功率时的最大效率转速、扭矩等参数

 Zmax=zeros(1,30);%设置零向量用来分别存储效率、转速、转矩
 Xmax=zeros(1,30);
 Ymax=zeros(1,30);
 for i=1:30
     Zmax(i)=10;
     for j=1:200
        for k=1:200
          if abs(xl(j)*yl(k)*pi/30000-P(i))<0.5 %类似于任务一，求离散点和等功率线交点，
              if (Z(k,j)>Zmax(i))
            
                  Zmax(i)=Z(k,j);
                  Xmax(i)=xl(j);
                  Ymax(i)=yl(k);
              end            
          end
        end
     end
 end
 

 p1=polyfit(Xmax,Ymax,3); %polyfit多项式拟合
 Xmax1=linspace(min(Xmax),max(Xmax),50);
 Ymax1=polyval(p1,Xmax1);
 p2=polyfit(P,Xmax,6); 
 p3=polyfit(P,Ymax,6); 
% % 由此得到电机最优曲线函数文件Tefit_mot()写入control.m文件
 
 xe=excel2(:,1);%同理，内燃机数据
 ye=excel2(:,2);
 ze=1000*3600./(46000*excel2(:,3));

 xl=linspace(min(xe),max(xe),200);
 yl=linspace(min(y),max(ye),200);
 [X,Y]=meshgrid(xl,yl);
 Z_mot=griddata(x,y,z,X,Y);
 Z_mot(isnan(Z_mot)==1)=0;
 Z_mot=Z_mot/100;
 Ze=griddata(xe,ye,ze,X,Y);
 Ze(isnan(Ze)==1)=0;%将外特性之外的点设置为零
 Z=Z_mot.*Ze;
 
% %计算每个功率下的最大效率对应的转速、转矩、功率和效率
 P=linspace(min(xl)*min(yl)*pi/30000,max(xl)*max(yl)*pi/30000-20,30);
% %各个功率最大效率点坐标
 Zmax=zeros(1,30);
 Xmax=zeros(1,30);
 Ymax=zeros(1,30);
 for i=1:30
     Zmax(i)=0.05;
     for j=1:200
        for k=1:200
          if abs(xl(j)*yl(k)*pi/30000-P(i))<0.5%求离散点和等功率线交点
              if (Z(k,j)>Zmax(i))
                  %记录最大效率点的be和坐标
                  Zmax(i)=Z(k,j);
                  Xmax(i)=xl(j);
                  Ymax(i)=yl(k);
              end            
          end
        end
     end
 end
 p1=polyfit(Xmax,Ymax,3);
 Xmax1=linspace(min(Xmax),max(Xmax),50);
 Ymax1=polyval(p1,Xmax1);
 p2=polyfit(P,Xmax,6);
 p3=polyfit(P,Ymax,6);
BEFORE =  [0 0 1 0]; %定义一个记录历史信息的变量，用于记录上一时刻发动机、电机工作状态
A=xlsread('EMMAP.xlsx');
clear EMMAP
EMMAP.Speed_Vector=[500:500:7000];
EMMAP.Torque_Vector=[-400:20:400];
for i=1:1:size(A,1)
    EMMAP.Efficiency_Table(A(i,1)/500,ceil(A(i,2)/20))=A(i,3);
 end
  EMMAP.Efficiency_Table=[fliplr(EMMAP.Efficiency_Table),...
      zeros(size(EMMAP.Speed_Vector,2),1),EMMAP.Efficiency_Table];
 EMMAP.Efficiency_Table(EMMAP.Efficiency_Table==0)=min(A(:,3));
 
 [meshgrid_Speed,meshgrid_Torque]=meshgrid(EMMAP.Speed_Vector, EMMAP.Torque_Vector);
 meshgrid_Efficiency = EMMAP.Efficiency_Table';
 Power_Max = max(A(:,1).*A(:,2)*2*pi/60);
 P = 1000:1000:100000;
 n = 500:100:7000;
 E = zeros(size(P));
 for i = 1:length(P)
     T = P(i)./n;
     e = griddata(meshgrid_Speed, meshgrid_Torque,meshgrid_Efficiency,n,T);
     E(i) = max(e);
 end
% %由此得到最优曲线写作函数文件efficiencyfit_mot.m
 