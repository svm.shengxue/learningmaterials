clc
clear all

excel= xlsread('2.0TD万有数据.xlsx');   %加载数据
Speed = excel(:,1);
Torque = excel(:,2);
BSFC = excel(:,3);

xline = linspace(850,4500,3650); %均分矢量，设置坐标分度
yline = linspace(0,200,200);
[X,Y] = meshgrid(xline,yline); %生成坐标
Z = griddata(Speed,Torque,BSFC,X,Y); % 对BSFC进行网格插值
Z(isnan(Z)) = inf;%无数值点记为inf
curve = [0,234,242,254,270,290,310,360,430,540];
contourf(X,Y,Z,curve,'ShowText','on');% 绘制万有特性图
xlabel('转速（r/min）');
ylabel('转矩（N·m）');
title('发动机BSFC万有特性图（g/kW·h）');
hold on

[Y1,X1]=find(Z==min(min(Z))); % 油耗最优点
text(X1+850,Y1,['o','油耗最优点','(',num2str(X1+850),',',num2str(Y1),')'],'color','g');

P = zeros(200,3650);% 计算每个网格点的功率
for i=1:200
    for j=1:3650
        P(i,j) = 2*pi*X(i,j)*Y(i,j)/60/1000; %功率=2Π·转速·转矩
        if Z(i,j) == inf
            P(i,j) = 0; % 外特性曲线以外的点功率记为0
        end
    end
end 
[Y2,X2] = find(P == max(max(P))); % 最高功率点
text(X2+850,Y2,['o','最高功率点','(',num2str(X2+850),',',num2str(Y2),')'],'color','r')

Y3 = max(Torque); % 最大扭矩点
X3 = Speed(Torque == Y3);
text(X3,Y3,['o','最大扭矩点','(',num2str(X3),',',num2str(Y3),')'],'color','b')

% 标定最低最高转速
text(min(Speed),170,['最低转速n=',num2str(min(Speed)),'r/mim'],'color','m');
text(max(Speed),170,['最高转速n=',num2str(max(Speed)),'r/mim'],'color','m');



%计算每个功率下的最小油耗对应的转速、转矩、功率和BSFC
a = 100;
bsfc = inf(a,1);
speed = zeros(a,1);
torque = zeros(a,1);
for k=1:a
    bsfc(k)=2000; 
    for i=1:200
        for j=1:3650
            if abs(P(i,j)-k) <= 0.5  %求离散点和等功率线交点
                if (Z(i,j)<bsfc(k)&&(Z(i,j)>0)) %用最小的BSFC点来取代，记录最小的BSFC点和此时其余参数的值
                    bsfc(k) = Z(i,j);
                    speed(k) = j;
                    torque(k) = i;
                end
            end
        end
    end
end

speed = 850.+speed;
speed(speed == 850) = [];
torque(torque == 0) = [];
plot(speed,torque,'w') % 绘制最优燃油的转矩转速曲线
pf1 = polyfit(speed,torque,2);% 绘制拟合曲线
pfx = 1200:50:4000;
pfy= polyval(pf1,pfx);
scatter(pfx,pfy,'w') % 绘制最优燃油的转矩转速拟合曲线散点图



