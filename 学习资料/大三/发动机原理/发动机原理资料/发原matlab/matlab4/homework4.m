clc
clear
A=importdata('cys-0.5度-anal_FLT_PCYL1-anal_FLT_PCYL1.csv');
Data=A.data;
[n,m]=size(Data);
D=zeros(n,2);
sum=zeros(n,1);
D(:,1)=Data(:,1);
for i=1:n                                %对缸压求和平均
    for j=2:m
        sum(i)=sum(i)+Data(i,j);
    end
    D(i,2)=sum(i)./50;
end
x=D(:,1);
y=D(:,2);
figure(1)
plot(x,y,'k-');                               %绘制平均后的纯压缩线，蓝色曲线
a=polyfit(x(1:80),y(1:80),3);            %对转角在-50度至-10度之间的数据用三次多项式拟合，得到四个系数
b=polyfit(x(81:121),y(81:121),2);        %对转角在-10度至+10度之间的数据用抛物线拟合，得到三个系数
c=polyfit(x(122:201),y(122:201),3);      %对转角在+10度至+50度之间的数据用三次多项式拟合，得到四个系数
y1=a(1,1).*(x(1:80).^3)+a(1,2).*(x(1:80).^2)+a(1,3).*x(1:80)+a(1,4);
y2=b(1,1).*(x(81:121).^2)+b(1,2).*x(81:121)+b(1,3);
y3=c(1,1).*(x(122:201).^3)+c(1,2).*(x(122:201).^2)+c(1,3).*x(122:201)+c(1,4);
y4=[y1;y2;y3];                            %三段拟合数据拼接
hold on
plot(x,y4,'r--');                         %绘制纯压缩线的拟合曲线，红色虚线
title('示功图p-φ曲线');
xlabel('曲轴转角φ/（°CA）');
ylabel('缸内压力p/bar');
legend('纯压缩线','拟合曲线')              %添加图例








