clc
clear
A=importdata('第一段缸压数据(2).txt');     %读取第一段缸压数据
B=importdata('第二段缸压数据(2).csv');     %读取第二段缸压数据
C=[A.data;B.data];                        %拼接后的p-φ数据
x=C(:,1);
y=C(:,2);
figure (1)
plot(x,y);
title('示功图p-φ曲线');
xlabel('曲轴转角φ/（°CA）');
ylabel('缸内压力p/bar');
D=0.074;r=0.043;L=0.133;Vc=0.000126387;
[n,m]=size(x);
z=zeros(n,1);
d=zeros(n,1);
for i=1:n          %角度转换到气缸容积
    d(i,1)=x(i,1)/180*pi;
    z(i,1)=D*D*pi/4*(L+r-r*cos(d(i,1))-sqrt(L*L-(r*sin(d(i,1)))*(r*sin(d(i,1)))))+Vc;  
end
figure (2)
plot(z,y);
title('示功图p-V曲线');
xlabel('气缸容积V/m^3');
ylabel('缸内压力p/bar');
S=[z,y];                             %气缸容积和缸压放入7200*2矩阵，拼接后的p-V数据
W1=0;W2=0;W3=0;W4=0;sum1=0;sum2=0;p1=zeros(n,1);p2=zeros(n,1);
for i=1:1800
    p1(i,1)=y(i,1);
    sum1=sum1+p1(i,1);
end
aver1=sum1/1800;                      %进气过程平均压力
for i=5401:7200
    p2(i,1)=y(i,1);
    sum2=sum2+p2(i,1);
end
aver2=sum2/1800;   %排气过程平均压力----这里结果是进气平均压力大于排气平均圧力
%近似梯形积分求功
for i=1:1800                  
    W1=W1+(S(i+1,1)-S(i,1))*S(i,2);      %进气做功
end
for i=1800:3600
    W2=W2+(S(i+1,1)-S(i,1))*S(i,2);      %压缩做功
end
for i=3600:5400
    W3=W3+(S(i+1,1)-S(i,1))*S(i,2);       %膨胀做功
end
for i=5400:7199
    W4=W4+(S(i+1,1)-S(i,1))*S(i,2);       %排气做功
end
Wpr=(W1+W4)*10^5;      %实际泵气功,单位为J
Wt=(W3+W2)*10^5;       %动力循环功，单位为J
save('date.mat','','C','S');
disp(Wpr);             %输出实际泵气功
disp(Wt);              %输出动力循环功
