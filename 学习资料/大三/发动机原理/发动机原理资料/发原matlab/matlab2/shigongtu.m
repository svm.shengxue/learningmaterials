clc
clear
A=importdata('第一段缸压数据(1).txt');   
B=importdata('第二段缸压数据(1).csv');
a=A.data;
b=B.data;
c=[a;b];
x=c(:,1);
y=c(:,2);
d=gradient(y)./gradient(x);
plot(x,d);
yyaxis right;
plot(x,y);
title('压力升高率曲线（蓝色）/示功图p-φ曲线（橙色）');
xlabel('曲轴转角φ/（°CA）');
ylabel('缸内压力p/bar');
save('date.mat','a','b','c','x','y');