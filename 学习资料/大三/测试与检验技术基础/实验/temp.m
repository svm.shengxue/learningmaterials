clc
clear
close all

data=importdata('23-05-17_20-06-32.txt');
T = data(:,1);
t = 0:0.01:(length(data)-1)*0.01;

figure(1)
plot(t, T)
xlabel('时间t(s)')
ylabel('温度T(℃)')
title('开水温度随时间变化曲线')