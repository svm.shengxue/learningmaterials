clear all;close all;clc;

% 系统模型参数
Kva=0.5;  % 电流环闭环增益
K1=830.6; % 被控对象电机参数
Kc=0.237; % 测速机增益
Beta=0.66; % 速度环反馈衰减系数
Kf=0.5; % 速度环反馈滤波器增益
T=0.0043; % 速度环反馈滤波器时间常数

R3=39e3;R6=100e3; % 速度环固定电阻

R4=40e3;  % 速度环比例控制器增益调节电阻
R5=R6+R4; % 速度环比例控制器反馈电阻

Kp=R5/R3; % 速度环比例控制器增益

Gjnum=Kp;Gjden=1;           % 校正环节传递函数-P控制
Kv=Kp*Kva*K1*Kc*Beta*Kf;    % 校正后开环静态增益Kv
zeta=1/(Kv*T)^0.5/2;        % 校正后闭环阻尼比zeta
fprintf('\nKp=%4.1f，Kv=%4.1f，zeta=%4.2f',Kp,Kv,zeta);

G0num=Kva*K1;G0den=[1 0];   % 控制对象传递函数
Gnum=conv(Gjnum,G0num);Gden=conv(Gjden,G0den);  % 校正后前向通道传递函数
Hnum=Kc*Beta*Kf;Hden=[T 1]; % 反馈通道传递函数
G0Hnum=conv(G0num,Hnum); G0Hden=conv(G0den,Hden);% 校正前开环传递函数
GHnum=conv(Gnum,Hnum); GHden=conv(Gden,Hden);% 校正后开环传递函数
[F0num F0den]=feedback(G0num,G0den,Hnum,Hden);% 校正前闭环传递函数
[Fnum Fden]=feedback(Gnum,Gden,Hnum,Hden);% 校正后闭环传递函数

[Gm0,Pm0,Wcg0,Wcp0] =margin(G0Hnum,G0Hden); %校正前稳定裕度,Gm0-幅值裕度,Pm0-相位裕度,Wcp0-剪切频率
fprintf('\n校正前: 相位裕量 Pm0=%4.0f     剪切频率Wcp0=%4.0f',Pm0,Wcp0);
[Gm,Pm,Wcg,Wcp] =margin(GHnum,GHden); %校正后稳定裕度,Gm-幅值裕度,Pm-相位裕度,Wcp-剪切频率
fprintf('\n校正后: 相位裕量 Pm=%4.0f     剪切频率Wcp=%4.0f\n',Pm,Wcp);

figure;bode(tf(G0Hnum,G0Hden),'b',tf(GHnum,GHden),'r');legend('校正前','P校正后');grid;title('开环Bode图');%开环伯德图
figure;bode(tf(F0num,F0den),'b',tf(Fnum,Fden),'r');legend('校正前','P校正后');grid;title('闭环Bode图');%闭环伯德图
figure;step(tf(F0num,F0den),'b',tf(Fnum,Fden),'r');legend('校正前','P校正后');grid;title('阶跃响应');ylabel('rotation rate (rad/s)');%阶跃响应

Vmax=11;
Vmin=-11;

Nmax=120;
Nmin=-120;

%simu_exp_speed_P;