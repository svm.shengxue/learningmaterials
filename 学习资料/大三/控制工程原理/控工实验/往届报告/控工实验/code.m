%%
%1.1
clc;close all;clear
num = [0,0,50];
den = [1e-5,0.1+1e-4,1];
step(num,den)
grid

%%
%1.2
clc;close all;clear
% s = zpk([],[-10,-1000,-10000],1e10);
% w = logspace(-2,8,100);
% bode(s,w)
% grid
% [Gm,Pm,Wcg,Wcp] = margin(s);
Kp = 50;Ki = 100;
% num = [50*Kp*0.001,50*Kp];
% den = [1e-8,0.0001101,0.1011,1+Kp];
% step(num,den)
% grid
num = [Kp*0.05,50*Kp+0.05*Ki,50*Ki];
den = [1e-8,0.0001101,0.1011,1+Kp,Ki];
step(num,den)
grid