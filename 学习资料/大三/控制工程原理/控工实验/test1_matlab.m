%%
%1.1
clc;close all;clear
num = [0,0,50];
den = [1e-5,0.1+1e-4,1];
step(num,den)
grid

%% 1.2
clc;close all;clear
G=tf([100],conv(conv([0.1,1],[0.001,1]),[0.0001,1]))

bode(G);grid;

[Gm,Pm,Wcg,Wcp]=margin(G)

%% 
%1.3
clc;close all;clear
data=sim('test1_2_5.slx')
B=data.simout;

(B);grid;

[Gm,Pm,Wcg,Wcp]=margin(B)

%% 1.3.1
close all;
clear;
clc;
figure 
s1=zpk([ ],[0 -1000 -10000],[2800000000])
bode(s1)
[Gm,Pm,Wcg,Wcp] = margin(s1)
grid on
title('伯德图')
figure 
