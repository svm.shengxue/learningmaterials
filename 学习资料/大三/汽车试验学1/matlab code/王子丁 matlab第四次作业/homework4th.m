clc
clear all
                                         
data = xlsread('Operating_Condition1.xls');                                      % 读取xls.1中的数据
data2 = xlsread('Operating_Condition2.xls');                                     % 读取xls.2中的数据
ang1 = data(:,1);
ang2 = data2(:,1);
p1 = mean(data(:,2:50),2);                                                       
p2 = mean(data2(:,2:31),2);                                                      
k1 = polyfit(ang1,p1,8);                                                         
k2 = polyfit(ang2,p2,8);
y1 = polyval(k1,ang1);
y2 = polyval(k2,ang2);
figure                                                                         
hold on
plot(ang1,p1,'m')
plot(ang1,y1,'r-.')
plot(ang2,p2,'k')
plot(ang2,y2,'b:')
hold off
title("缸压曲线和拟合曲线")
xlabel("曲轴转角/°")     
ylabel("压力/bar") 
legend("缸压曲线（工况一）","拟合曲线（工况一）","缸压曲线（工况二)","拟合曲线(工况二)")