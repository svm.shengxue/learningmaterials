喷油提前角过小时，将导致燃烧过程延后过多，最高压力值下降，从而使柴油机热效率明显下降。
因此若要保证柴油机有良好的性能，必须选定最佳喷油提前角。