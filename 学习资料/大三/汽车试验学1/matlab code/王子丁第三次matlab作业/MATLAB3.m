clc
clear all
                                          
data = importdata('转角-缸压_第一段数据.csv');                                               
data2 = importdata('转角-缸压_第二段数据.txt');                                             
title1 = {'转角 [°CA]','缸压 [bar]'};
xlswrite('转角-缸压第一二阶段整合.xlsx',[title1;num2cell(data.data);num2cell(data2.data)]);       
pre = xlsread('转角-缸压第一二阶段整合.xlsx');
ang = pre(:,1);
pres = pre(:,2);
r = 83.3;                                                                                  
l = 261;                                                                                 
d = 126.5;                                                                                  
fuel_efficiency = 210.84;                                                                   
H = 42.5;                                                                                   
v = 1120;                                                                                   
T = 570;                                                                                   
p = 17.3;
g = 6;
c = 4;
Vs=pi/4*d.^2*2*r;                                                                           
Vc=Vs/(p-1);                                                                                
s = (r+l-r.*cos(ang/180*pi)-sqrt(l*l-r*r.*sin(ang/180*pi).*sin(ang/180*pi)));
Volume_gas=(Vc+pi/4*s.*d.^2)/10^6;                                                          
plot(Volume_gas,pres)
title("发动机p-V图")
xlabel("气缸容积(L)")     
ylabel("压力(bar)")  


W1 = trapz(Volume_gas(1:1800,:),pres(1:1800,:))*100;                               
W2 = trapz(Volume_gas(5401:7200,:),pres(5401:7200,:))*100;                                
W3 = trapz(Volume_gas(1801:3600,:),pres(1801:3600,:))*100;                                
W4 = trapz(Volume_gas(3601:5400,:),pres(3601:5400,:))*100;                                 
fprintf('进气冲程做功 %f J\n',W1);
fprintf('排气冲程做功 %f J\n',W2);
fprintf('压缩冲程做功 %f J\n',W3);
fprintf('膨胀冲程做功 %f J\n\n',W4);


Wi=W2+W3;
B=T*2*pi*v*fuel_efficiency/(10^3*10^3*3600);
g_b=B*c/(2*g*v);  
n_it=Wi/(g_b*H*10^6); 
fprintf('指示热效率为 %f \n',n_it);