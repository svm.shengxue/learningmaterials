clc

clear all
load data.mat  %%加载数据


x=data.CA;    %%x轴
y=data.bar;   %%y轴


plot(x,y)  %% 做二维线图

dy=diff(y)./diff(x);  %%求导
hold on 
plot(x(1:end-1),dy,'r') %%做二维线图
legend('压力-转角图','压力升高率-转角图')
xlabel('曲轴转角(°)')
ylabel('压力(bar)/压力升高率(bar/°)')